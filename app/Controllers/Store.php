<?php

namespace App\Controllers;

class Store extends BaseController
{
    public function pricing(): string
    {

        $data = [
            'title' => 'SPEEDWORK',
            'pricing' => $this->pricingModel->getAllPricingStore(),
            'template_name' => $this->pricingModel->getTemplateName()
        ];

        return view('store-pricing/pricing', $data);
    }

    // public function template($temp_name): string
    // {

    //     $data = [
    //         'title' => 'SPEEDWORK',
    //         'temp_name' => $this->templateModel->getTemplateName(),
    //         'template_name' => $this->templateModel->getTemplatePriceByTemplateName($temp_name)
    //     ];

    //     return view('store-pricing/template', $data);
    // }
}
