<?php

namespace App\Controllers;

use App\Actions\StorePricing\FetchIndexDataAction;
use App\Actions\StorePricing\UpdateStoreProductsAction;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use PhpParser\Node\Expr\FuncCall;

class ProductControllers extends BaseController
{
    public function index()
    {
        return view('store-pricing/pricing');
    }

    public function loadData()
    {
        return (new FetchIndexDataAction)->execute($this->request->getPost());
    }

    public function update()
    {
        return (new UpdateStoreProductsAction)->execute($this->request->getPost());
    }
}
