<?php

namespace App\Actions\StorePricing;

class FetchStoreProductAction
{
    private $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    public function execute($data)
    {
        $searchColumns = [
            'sp.store_code',
            'sp.product_code',
            'sp.sku_code',
            'sp.product_name',
        ];

        $query = $this->db->table('t_store_product as sp')
            ->select('
                sp.id,
                sp.store_code,
                sp.product_code,
                sp.sku_code,
                sp.product_name,
                sp.product_price,
                sp.store_price,
                sp.discount_price');

        // if (filled($data['category'])) {
        //     $query = $query->where('sp.category_name', $data['category']);
        // }

        if (filled($data['store'])) {
            $query = $query->where('sp.store_code', $data['store']);
        }

        if (filled($data['search']['value'])) {
            foreach ($searchColumns as $key => $column) {
                if ($key === 0) {
                    $query = $query->like($column, $data['search']['value']);
                } else {
                    $query = $query->orLike($column, $data['search']['value']);
                }
            }
        }

        return $query;
    }
}
