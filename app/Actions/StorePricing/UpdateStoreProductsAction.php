<?php

namespace App\Actions\StorePricing;

class UpdateStoreProductsAction
{
    private $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    public function execute($data)
    {
        // $statusData = isset($data['status']) ? '1' : '0';

        $response = [
            'status' => 'failed',
            'msg' => 'Gagal update harga jual!'
        ];

        $update = $this->db->table('t_store_product')
            ->where('id', $data['id'])
            ->update([
                'store_price' => str_replace('.', '', $data['store_price'])
            ]);

        if ($update) {
            $response['status'] = 'success';
            $response['msg'] = 'Harga jual berhasil diupdate';
        } else {
            $response['error'] = $this->db->error();
        }

        return json_encode($response);
    }
}
