<?php

namespace App\Actions\StorePricing;

class FetchIndexDataAction
{
    public function execute($request)
    {
        $products = $this->getDtData($request);
        $totalCount = $this->countDtResult($request);

        $data = [];

        foreach ($products as $product) {
            $row = [];
            $action = '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-edit" data-id="' . $product->id . '" data-store-price="' . $product->store_price . '" data-toggle="modal" data-target="#storePrice">
            <i class="fa fa-edit"></i>
            </a>';
            $row[] = $action;
            $row[] = $product->store_code;
            $row[] = $product->product_code;
            $row[] = $product->sku_code;
            $row[] = $product->product_name;
            $row[] = number_format($product->product_price, 0, ',', '.');
            $row[] = '<div class="product-store-price form-control">' . number_format($product->store_price, 0, ',', '.') . '</div>';
            $row[] = number_format($product->discount_price, 0, ',', '.');
            $data[] = $row;
        }

        $output = [
            'draw' => $request['draw'],
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $totalCount,
            'data' => $data
        ];

        return json_encode($output);
    }

    private function getDtData($data)
    {
        $query = (new FetchStoreProductAction)->execute($data);

        if ($data['length'] != -1) {
            $query = $query->limit($data['length'], $data['start']);
        }

        return $query->get()->getResult();
    }

    private function countDtResult($data)
    {
        $query = (new FetchStoreProductAction)->execute($data);

        return $query->countAllResults();
    }
}
