<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
// $routes->get('/v1', 'Store::pricing');
$routes->addRedirect('v1', 'v1/store-pricing');
$routes->group('v1', static function ($routes) {
    $routes->group('store-pricing', static function ($routes) {
        $routes->get('', 'ProductControllers::index');
        $routes->post('load-data', 'ProductControllers::loadData');
        $routes->post('update/(:segment)', 'ProductControllers::update/$1');
    });
});
// $routes->get('/template/(:any)', 'Store::template/$1');