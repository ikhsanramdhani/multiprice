<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="https://capp.speedwork.id/favicon.ico" type="image/x-icon">

    <title>SPEEDWORK</title>

    <!-- Custom fonts for this template -->
    <link href="<?= base_url('style/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
    <!-- <link href="style/fa.css" rel="stylesheet" type="text/css"> -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.bootstrap4.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Custom styles for this template -->
    <link href="<?= base_url('style/styles.css') ?>" rel="stylesheet">
    <link href="<?= base_url('style/toast.css') ?>" rel="stylesheet">
    <link href="<?= base_url('script/dist/css/select2.min.css') ?>" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="<?= base_url('style/datatables.css') ?>" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">


        <?= $this->include('layout/sidebar') ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <!-- <form class="form-inline"> -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- </form> -->

                    <!-- Topbar Search -->
                    <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <div class="bg-white py-2 px-1 collapse-inner rounded">
                                <img src="https://capp.speedwork.id/img/LogoSpeedwork.d8e63c21.png" style="width: 13vw; height:6vh;" title="Speedwork Autocare" alt="c">
                            </div>
                        </div>
                    </div>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">ADMIN</span>
                                <img class="img-profile rounded-circle" src="">
                            </a>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <?= $this->renderSection('content'); ?>
            </div>
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>&copy; <?= date('Y') ?> PT Speedwork Solusi Utama. All Rights Reserved.</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('script/jquery.js') ?>"></script>
    <script src="<?= base_url('script/bootstrap.js') ?>"></script>
    <script src="<?= base_url('script/jquery-e.js') ?>"></script>
    <script src="<?= base_url('script/scripts.js') ?>"></script>
    <script src="<?= base_url('script/jquery-datatables.js') ?>"></script>
    <script src="<?= base_url('script/datatables.js') ?>"></script>
    <script src="<?= base_url('script/demo.js') ?>"></script>
    <script src="<?= base_url('script/dist/js/select2.min.js') ?>"></script>

    <!-- GLOBAL SCRIPT -->
    <script>
        let csrfToken = '<?php echo csrf_hash() ?>'
        let baseUrl = '<?php echo base_url() ?>'

        function errorAlert() {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast',
                },
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
            })

            return Toast.fire({
                icon: 'error',
                title: 'Error',
            })
        }

        function successAlert() {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast',
                },
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true,
            })

            return Toast.fire({
                icon: 'success',
                title: 'Success',
            })
        }

        function submitRequest(data, reloadUrl = null) {
            // $('.btnSubmit').attr('disabled', true)
            $.ajax({
                url: data.url,
                type: data.method,
                data: data.data,
                dataType: 'json',
                success: function(response) {
                    // $('.btnSubmit').removeAttr('disabled')
                    if (response.status === 'success') {
                        // successAlert(response.msg)
                        //     .then(() => {
                        //         if (reloadUrl !== null) {
                        //             window.location.href = reloadUrl
                        //         } else {
                        //             window.location.reload()
                        //         }
                        //     })
                        console.log(response);
                        status = true;
                    } else {
                        console.log(response);
                        // let errorMessage = (appEnv === 'production' ? response.error.message : response.error)
                        // errorAlert(errorMessage, true)
                    }

                },
                error: function(err) {
                    // $('.btnSubmit').removeAttr('disabled')
                    console.log(err);
                }
            })
        }
    </script>

    <?= $this->renderSection('script') ?>
</body>

</html>