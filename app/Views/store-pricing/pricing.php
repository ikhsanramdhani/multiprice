<?= $this->extend('layout/layout'); ?>

<?= $this->section('content'); ?>

<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Pricing Table</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <button type="button" class="btn btn-sm btn-info btn-pill" title="Filter" style="float: left" data-toggle="modal" data-target="#filterModal">
                        <i class="fa fa-filter "></i>
                    </button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="store-pricing" width="100%" cellspacing="0">
                    <thead class="bg-light">
                        <tr>
                            <th></th>
                            <th>Store Code</th>
                            <th>Product Code</th>
                            <th>SKU Code</th>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Store Price</th>
                            <th>Discount Price</th>
                        </tr>
                    </thead>
                    <tfoot class="bg-light">
                        <tr>
                            <th></th>
                            <th>Store Code</th>
                            <th>Product Code</th>
                            <th>SKU Code</th>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Store Price</th>
                            <th>Discount Price</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <!-- <tr>
                            <td>100030000</td>
                            <td>102ZENEOS2</td>
                            <td>10K54</td>
                            <td>Ban GT Radial</td>
                            <td>Rp 879.000</td>
                            <td><a href="#">Rp 879.000</a></td>
                            <td>Rp 0</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalTitle">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Store</label>
                    <?= selectStore('store-filter') ?>
                </div>
                <!-- <div class="form-group">
                    <label>Kategori</label>
                    <?= selectProductCategory('category-filter') ?>
                </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save-modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="storePrice" tabindex="-1" role="dialog" aria-labelledby="storePriceTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="storePriceTitle">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id" value="">
                <div class="form-group">
                    <label>Store Price</label>
                    <input type="text" id='store-price' class="form-control" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save-modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>
<?= $this->section('script') ?>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function() {
        $('#store-filter').select2({
            dropdownParent: $('#filterModal')
        })
        data = function(d) {
            d._token = csrfToken
            // d.category = $('#filterModal #category-filter').val(),
            d.store = $('#filterModal #store-filter').val()
        }
        let productTable = $('#store-pricing').DataTable({
            info: true,
            lengthChange: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: `${baseUrl}/v1/store-pricing/load-data`,
                type: 'POST',
                data: data,
                dataType: 'json'
            },
            columnDefs: [{
                targets: [0],
                orderable: false
            }],
            language: {
                emptyTable: 'Data tidak ditemukan!',
                searchPlaceholder: "Cari...",
            }
        })

        $('#filterModal').on('show.bs.modal', function() {
            $('#filterModal .btn-save-modal').click(function(e) {
                e.preventDefault();

                productTable.ajax.reload()
                $('#filterModal').modal('hide')
            })
        })

        $('#storePrice').on('show.bs.modal', function() {
            $('#store-price').on('change click keyup input paste', (function(event) {
                $(this).val(function(index, value) {
                    return '' + value.replace(/(?!\.)\D/g, "")
                        .replace(/(?<=\.*)\./g, "")
                        .replace(/(?<=\\d\d).*/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                });
            }));
            $('#storePrice .btn-save-modal').click(function(e) {
                console.log($('#store-price').val());
                data = {
                    'url': `${baseUrl}/v1/store-pricing/update/${$('#id').val()}`,
                    'method': 'POST',
                    'table': 'productTable',
                    'data': {
                        'id': $('#id').val(),
                        'store_price': $('#store-price').val()
                    }
                }
                submitRequest(data)
                productTable.ajax.reload()
                $('#storePrice').modal('hide')
                successAlert()
            })
        })

    })

    $(document).on('click', '.btn-edit', function() {
        $('#id').val($(this).data('id'))
        $('#store-price').val($(this).data('store-price'))
    })
</script>
<?= $this->endSection(); ?>