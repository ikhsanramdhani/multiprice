<?php

namespace App\Models;

use CodeIgniter\Model;
// use \phpseclib\Net\SSH2;

class PricingModel extends Model
{

    protected $table = 't_store_product';
    protected $allowedFields = [
        'store_price', 'discount_price'
    ];

    public function getDbConnection()
    {

        // configure ssh connect
        // $ssh = new SSH2(env('SSH-HOSTNAME'));
        // if (!$ssh->login(env('SSH-USERNAME'), env('SSH-PASSWORD'))) {
        //     # code...
        //     die("Connect SSH Failed...");
        // }

        $dbConfig = [
            'DSN'          => '',
            'hostname'     => env('HOSTNAME'),
            'username'     => env('USER'),
            'password'     => env('PASSWORD'),
            'database'     => 'db_sw_pricing',
            'DBDriver'     => 'MySQLi',
            'DBPrefix'     => '',
            'pConnect'     => false,
            'DBDebug'      => true,
            'charset'      => 'utf8',
            'DBCollat'     => 'utf8_general_ci',
            'swapPre'      => '',
            'encrypt'      => false,
            'compress'     => false,
            'strictOn'     => false,
            'failover'     => [],
            'port'         => env('PORT'),
            'numberNative' => false,
        ];

        $localPort = 3306;
        $remotePort = 3306;

        $ssh->forward($localPort, 'localhost', $remotePort);

        $dbConfig['hostname'] = '127.0.0.1';
        $dbConfig['port'] = $localPort;

        $db = db_connect($dbConfig);

        return $db;

    }

    public function getAllPricingStore()
    {   
        $q = "select * from t_store_product where status='1' limit 10";
        return $this->db->query($q)->getResultArray();
        // return $this->findAll(60);
    }

    public function getTemplateName()
    {
        $q = "select template_name, area_name from t_pricing_template_name";
        return $this->db->query($q)->getResultArray();
    }

}