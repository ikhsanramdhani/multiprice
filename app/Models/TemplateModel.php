<?php

namespace App\Models;

use CodeIgniter\Model;

class TemplateModel extends Model
{

    protected $table = 't_pricing_template';
    protected $allowedFields = [
        'store_price', 'discount_price'
    ];

    // public function getTemplateStore($template_name = false)
    // {

    //     if ($template_name == false) {
    //         # code...
    //         return $this->findAll();
    //     }

    //     return $this->where(['template_name' => $template_name])->first();

    // }

    public function getTemplateName()
    {
        $q = "select template_name, area_name from t_pricing_template_name";
        return $this->db->query($q)->getResultArray();
    }

    public function getTemplatePriceByTemplateName($template_name)
    {
        $q = "select t_pim_sku.sku_name, t_pricing_template.sku_code, t_pricing_template.product_price, t_pricing_template.store_price, 
            t_pricing_template.discount_price, t_pricing_template_name.area_name from t_pricing_template 
            left join t_pim_sku on t_pim_sku.sku_code = t_pricing_template.sku_code 
            left join t_pricing_template_name on t_pricing_template_name.template_name = t_pricing_template.template_name 
            where t_pricing_template.template_name = '{$template_name}' ";
        return $this->db->query($q)->getResultArray();
    }

}