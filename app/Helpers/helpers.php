<?php

function selectProductCategory(string $id, $selected = null)
{
    $html = '<select id="' . $id . '" name="' . $id . '" class="form-control product-category" width="100%">';
    $html .= '<option value="">-- Pilihan --</option>';

    $db = \Config\Database::connect();

    $productCategories = $db->table('t_store_product')
        ->select('category_name')
        ->groupBy('category_name')
        ->get()
        ->getResult();

    foreach ($productCategories as $category) {
        $isSelected = $selected == $category->category_name ? 'selected' : '';

        $html .= '<option value="' . $category->category_name . '" ' . $isSelected . '>' . $category->category_name . '</option>';
    }

    $html .= '</select>';

    return $html;
}

function selectStore(string $id, $selected = null)
{
    $html = '<select id="' . $id . '" name="' . $id . '" class="form-control" style="width:100%">';
    $html .= '<option value="">-- Pilihan --</option>';

    $db = \Config\Database::connect();

    $stores = $db->table('t_store_product as sp')
        ->select('sp.store_code,str.store_name')
        ->join('sto_store as str', 'str.store_code=sp.store_code')
        ->groupBy('sp.store_code,str.store_name')
        ->get()
        ->getResult();

    foreach ($stores as $store) {
        $isSelected = $selected == $store->store_code ? 'selected' : '';

        $html .= '<option value="' . $store->store_code . '" ' . $isSelected . '>' . $store->store_name . ' [' . $store->store_code . ']</option>';
    }

    $html .= '</select>';

    return $html;
}


function blank($value)
{
    if (is_null($value)) {
        return true;
    }

    if (is_string($value)) {
        return trim($value) === '';
    }

    if (is_numeric($value) || is_bool($value)) {
        return false;
    }

    if ($value instanceof Countable) {
        return count($value) === 0;
    }

    return empty($value);
}

function filled($value)
{
    return !blank($value);
}
